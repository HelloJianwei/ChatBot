from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
'''deepThought = ChatBot(
            "deepThought",
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter')'''
deepThought = ChatBot("deepThought")
deepThought.set_trainer(ChatterBotCorpusTrainer)
# 使用中文语料库训练它
deepThought.train("chatterbot.corpus.chinese")  # 语料库
#deepThought.train("chatterbot.corpus.english")
#deepThought.train("C:/Users/Jianwei/Anaconda3/Lib/site-packages/chatterbot_corpus/data/simsimi/")